-- LUALOCALS < ---------------------------------------------------------
local ItemStack, ipairs, minetest, nodecore, pairs, string, vector
    = ItemStack, ipairs, minetest, nodecore, pairs, string, vector
local string_lower
    = string.lower
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local function capture(pos, placer)
	if not placer then return end
	local below = vector.offset(pos, 0, -1, 0)
	local bnode = minetest.get_node(below)
	if minetest.get_item_group(bnode.name, modname) == 0 then return end

	local node = minetest.get_node(pos)
	nodecore.set_loud(below, node)
	minetest.remove_node(pos)

	-- Delay returning item briefly in case the item would
	-- be placed in the wielded stack, which is subject to
	-- replacement by the engine after returning here.
	minetest.after(0, function()
			local stack = nodecore.give_item(placer,
				ItemStack(bnode.name), "main")
			if not stack:is_empty() then
				nodecore.item_eject(pos, stack)
			end
		end)

	local def = minetest.registered_nodes[bnode.name]
	if not def then return end
	nodecore.digparticles(def, {
			time = 0.05,
			amount = 20,
			minpos = vector.offset(below, -0.25, -0.4, -0.25),
			maxpos = vector.offset(below, 0.25, -0.4, 0.25),
			minvel = {x = -2, y = 2, z = -2},
			maxvel = {x = 2, y = 2, z = 2},
			minacc = {x = 0, y = -8, z = 0},
			maxacc = {x = 0, y = -8, z = 0},
			minexptime = 0.25,
			maxexptime = 0.5,
			collisiondetection = false,
			minsize = 1,
			maxsize = 6,
			attract = {
				kind = "point",
				origin = vector.offset(below, 0, -0.65, 0),
				strength = -5
			}
		})
end

local chess_alias = {
	concrete_cloudstone = "white",
	concrete_coalstone = "black",
}

local function reg(name, shape, pattern, basedef)
	local piecebase = modname .. ":" .. string_lower(shape) .. "_"
	local piecename = piecebase .. string_lower(name)
	if minetest.registered_nodes[piecename] then return end
	local desc = basedef.description or name
	minetest.register_node(":" .. piecename, {
			description = desc .. " " .. shape,
			drawtype = "mesh",
			mesh = modname .. "_" .. string_lower(shape) .. ".obj",
			selection_box = {
				type = "fixed",
				fixed = {-3/8, -0.5, -3/8, 3/8, 0.5, 3/8}
			},
			tiles = {
				basedef.tiles[1] .. "^[colorize:#808080:32"
			},
			paramtype = "light",
			paramtype2 = "4dir",
			groups = {
				[modname] = 1,
				snappy = 1,
				falling_node = 1,
				falling_repose = 1
			},
			sounds = basedef.sounds,
			after_place_node = capture,
			mapcolor = basedef.mapcolor,
		})
	if chess_alias[name] then
		minetest.register_alias(piecebase .. chess_alias[name], piecename)
	end
	nodecore.register_craft({
			label = "chisel " .. string_lower(desc) .. " " .. string_lower(name),
			action = "pummel",
			toolgroups = {thumpy = 3},
			normal = {y = 1},
			indexkeys = {"group:chisel"},
			nodes = {
				{
					match = {
						lode_temper_tempered = true,
						groups = {chisel = 2}
					},
					dig = true
				},
				{
					y = -1,
					match = "nc_concrete:" .. name .. "_" .. pattern,
					replace = piecename
				}
			}
		})
end

local function buildall()
	for _, v in ipairs(nodecore.registered_concrete_etchables) do
		local basedef = v.name and v.basename and minetest.registered_nodes[v.basename]
		if basedef then
			for shape, pattern in pairs({
					Pawn = "bricky",
					Knight = "vermy",
					Bishop = "hashy",
					Rook = "boxy",
					Queen = "iceboxy",
					King = "bindy",
				}) do
				reg(v.name, shape, pattern, basedef)
			end
		end
	end
end

do
	local old_reg_etch = nodecore.register_concrete_etchable
	local function helper(...)
		buildall()
		return ...
	end
	nodecore.register_concrete_etchable = function(...)
		return helper(old_reg_etch(...))
	end
end

buildall()
