Chisel decorative, easy to move chess pieces out of etched (patterned) tarstone and cloudstone.

Playing chess itself is currently almost entirely manual; no chess engines, rules, or other aids are programmed into the mod.  The pieces can be used for anything: playing chess on a server, setting up chess puzzles, or just as decoration.  Board design/building is left entirely up to the players.

Placing a chess piece atop any other (placed) piece will "capture" that piece: the new piece replaces the original one, and the original piece is put into the capturing player's inventory (or ejected above if there is no room).